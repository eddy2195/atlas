//
//  JSONStorageService.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

final class JSONStorageService {
    
    private let documentDirectoryFileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(Constants.filename)
    
    var databaseExists: Bool {
        set {}
        get {
            guard let fileURL = documentDirectoryFileURL else { return false }
            return FileManager.default.fileExists(atPath: fileURL.path) }
    }
    
    func read() -> OperationResult<Data> {
        guard let fileURL = documentDirectoryFileURL else {
            let error = NSError(domain: "", code: Constants.ErrorCodes.noAccessToDocumentsDirectory.rawValue, userInfo: [NSLocalizedDescriptionKey : Constants.ErrorTexts.noAccessToDocumentsDirectory.rawValue.localized()])
            return .failed(error)
        }
        do {
            let data = try Data(contentsOf: fileURL)
            return .successful(data)
        } catch let readError {
            return .failed(readError)
        }
    }
    
    func write(data: Data) {
        guard let fileURL = documentDirectoryFileURL else { return }
        try? data.write(to: fileURL, options: .atomic)
    }
}
