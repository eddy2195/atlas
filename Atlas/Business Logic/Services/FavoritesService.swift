//
//  FavoritesService.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

final class FavoritesService {
        
    static let shared = FavoritesService()
    
    private let keyStore = NSUbiquitousKeyValueStore()
    private (set) lazy var countriesAlphaCodes: [String] = {
        guard let storedFavories = keyStore.array(forKey: Constants.favoriteCountriesKey) as? [String] else { return [String]() }
        return storedFavories
    }()
    
    func add(country: Country) {
        countriesAlphaCodes.append(country.alpha3Code)
        updateKeystore()
    }
    
    func remove(country: Country) {
        countriesAlphaCodes.removeAll { $0 == country.alpha3Code }
        updateKeystore()
    }
    
    private func updateKeystore() {
        keyStore.set(countriesAlphaCodes, forKey: Constants.favoriteCountriesKey)
        keyStore.synchronize()
    }
    
}
