//
//  DataSource.swift
//  Atlas
//
//  Created by Alex on 12/29/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

protocol FetchDelegate {
    func fetchingFinished(error: Error?)
}

enum OperationResult<T> {
    case successful(T)
    case failed(Error?)
}

enum Source {
    case filesystem, server
    
    fileprivate var provider: CountryDataProvider {
        get {
            switch self {
                case .filesystem: return FilesystemDataProvider()
                case .server: return ServerDataProvider()
            }
        }
        set {}
    }
}

final class DataSource {
    
    private (set) var databaseExists: Bool {
        set {}
        get { return storage.databaseExists }
    }
    
    private (set) var regions = NSOrderedSet()
    
    lazy private var storage: JSONStorageService = {
        return JSONStorageService()
    }()
    
    private var countries: [Country]? {
        didSet {
            guard let countries = countries else { return }
            self.regions = NSOrderedSet(array: countries.map{ $0.region })
        }
    }
    
    private var fetchDelegate: FetchDelegate?
    
    init(with fetchDelegate: FetchDelegate) {
        self.fetchDelegate = fetchDelegate
    }
    
    func fetch(from source: Source = .server) {
        source.provider.fetch(with: storage, completion: { [weak self] (result) in
            guard let `self` = self else { return }
            var fetchingError: Error?
            switch result {
                case .successful(let countries):
                    self.countries = countries
                case .failed(let error):
                    fetchingError = error
            }
            DispatchQueue.main.async {
                self.fetchDelegate?.fetchingFinished(error: fetchingError)
            }
        })
    }
    
    func countries(in region: String) -> [Country] {
        guard let countries = countries else { return [Country]() }
        return countries.filter { return $0.region == region }
    }
    
    func country(withCode code: String) -> Country? {
        guard let countries = countries else { return nil }
        return countries.filter{ return $0.alpha3Code == code }.first
    }
    
    func countries(withName name: String) -> [Country] {
        guard let countries = countries else { return [Country]() }
        return countries.filter { $0.name.lowercased().contains(name.lowercased()) }
    }
    
    func neigbours(of country: Country) -> [Country]? {
        var neighbours = [Country]()
        country.borders.forEach {
            guard let neighbour = self.country(withCode: $0) else { return }
            neighbours.append(neighbour)
        }
        return neighbours
    }
    
}
