//
//  Constants.swift
//  Atlas
//
//  Created by Alex on 1/31/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

struct SharedConstants {
    static let mainStoryboardName = "Main"
}

extension ServerDataProvider {
    struct Constants {
        static let scheme = "https";
        static let host = "restcountries.eu";
        static let path = "/rest/v2/all";
        static let fieldsParameterName = "fields";
        
        static let requiredFields: [Field] = [.name, .capital, .currencies, .nativeName, .languages, .latlng, .region, .borders, .alpha3Code, .alpha2Code]
        
        enum Field: String {
            case name, capital, currencies, nativeName, languages, latlng, region, borders, alpha3Code, alpha2Code
        }
    }
}

extension JSONStorageService {
    struct Constants {
        static let filename = "data.json"
        
        enum ErrorTexts: String {
            case noAccessToDocumentsDirectory = "Have no access to local storage"
        }
        
        enum ErrorCodes: Int {
            case noAccessToDocumentsDirectory = 403
        }
    }
}

extension FavoritesService {
    struct Constants {
        static let favoriteCountriesKey = "favoriteCountries"
    }
}

extension LoadingViewController {
    struct Constants {
        static let fetchingDatabaseString = "Fetching database..."
        static let regionsSegueIdentifier = "showRegions"
    }
}

extension CountryDetailsTableViewController {
    struct Constants {
        static let boardsWithText = "Boards with:"
        static let numberOfSections = 2
        static let numberOfCellsInDetailsSection = 1
        
        enum Section: Int {
            case details, neighbours
        }
    }
}

extension RegionsTableViewController {
    struct Constants {
        static let regionCellReuseIdentifier = "RegionCell"
        static let noNameRegion = "Unnamed"
    }
}

extension FlagsProvider {
    struct Constants {
        static let notFoundFlag = "🏳️"
        static let filename = "Flags"
        static let fileExtension = "json"
    }
}
