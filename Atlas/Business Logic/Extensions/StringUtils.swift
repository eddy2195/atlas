//
//  StringUtils.swift
//  Atlas
//
//  Created by Alex on 1/30/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

extension String {
    func localized(_ comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? self)
    }
}
