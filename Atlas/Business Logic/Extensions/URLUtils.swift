//
//  URLUtils.swift
//  Atlas
//
//  Created by Alex on 1/31/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

extension URL {
    static func with(fields: [ServerDataProvider.Constants.Field]) -> URL? {
        var components = URLComponents()
        components.scheme = ServerDataProvider.Constants.scheme
        components.host = ServerDataProvider.Constants.host
        components.path = ServerDataProvider.Constants.path
        
        if !fields.isEmpty {
            let fieldsQuery = URLQueryItem(name: ServerDataProvider.Constants.fieldsParameterName, value: fields.map{$0.rawValue}.joined(separator: ";"))
            components.queryItems = [fieldsQuery]
        }
        
        return components.url
    }
}
