//
//  CountryDataProviderProtocol.swift
//  Atlas
//
//  Created by Alex on 1/30/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

typealias DataProviderResult = (_ result: OperationResult<[Country]>) -> ()

protocol CountryDataProvider {
    func fetch(with storageService: JSONStorageService, completion: @escaping DataProviderResult)
}
