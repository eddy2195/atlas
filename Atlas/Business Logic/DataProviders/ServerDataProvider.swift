//
//  ServerDataProvider.swift
//  Atlas
//
//  Created by Alex on 1/30/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

final class ServerDataProvider: CountryDataProvider {
    
    func fetch(with storageService: JSONStorageService, completion: @escaping DataProviderResult) {
        guard let url = URL.with(fields: Constants.requiredFields) else {
            assertionFailure("Invalid URL")
            return
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                completion(.failed(error))
                return
            }
            do {
                let countries = try JSONDecoder().decode([Country].self, from: dataResponse)
                storageService.write(data: dataResponse)
                completion(.successful(countries))
            } catch let parsingError {
                completion(.failed(parsingError))
            }
        }
        task.resume()
    }
    
}
