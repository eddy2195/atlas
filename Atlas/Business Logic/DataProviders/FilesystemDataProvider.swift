//
//  FilesystemDataProvider.swift
//  Atlas
//
//  Created by Alex on 1/30/19.
//  Copyright © 2019 SH. All rights reserved.
//

import Foundation

final class FilesystemDataProvider: CountryDataProvider {
    
    func fetch(with storageService: JSONStorageService, completion: @escaping DataProviderResult) {
        let readResult = storageService.read()
        
        switch readResult {
        case .successful(let data):
            do {
                let countries = try JSONDecoder().decode([Country].self, from: data)
                completion(.successful(countries))
            } catch let parsingError {
                completion(.failed(parsingError))
            }
        case .failed(let error):
            completion(.failed(error))
        }
    }
    
}
