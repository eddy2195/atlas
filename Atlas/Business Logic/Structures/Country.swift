//
//  Country.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

struct Country: Decodable {
    let alpha2Code: String
    let alpha3Code: String
    let borders: [String]
    let capital: String
    let currencies: [Currency]
    let languages: [Language]
    let latlng: [Double]? //Some countries are returned without coordinates
    let name: String
    let nativeName: String
    let region: String
    
    var flag: String  {
        get { return FlagsProvider.shared.flag(for: alpha2Code) }
        set {}
    }
    
}
