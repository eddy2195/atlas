//
//  Currency.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

// For some countries we may receive empty code, name or symbol.
struct Currency: Decodable {
    let code: String?
    let name: String?
    let symbol: String?
}
