//
//  Language.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

struct Language: Decodable {
    let name: String
    let nativeName: String
}
