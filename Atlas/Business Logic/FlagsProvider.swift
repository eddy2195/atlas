//
//  FlagsProvider.swift
//  Atlas
//
//  Created by Alex on 12/29/18.
//  Copyright © 2018 SH. All rights reserved.
//

import Foundation

final class FlagsProvider {
    
    private struct Flag: Decodable {
        var code: String
        var emoji: String
    }
    
    static let shared = FlagsProvider()
    
    private var flags = [Flag]()
    
    init() {
        guard let flagsFilePath = Bundle.main.url(forResource: Constants.filename, withExtension: Constants.fileExtension) else {
            assertionFailure("No flags file")
            return
        }
        guard let flagsData = try? Data(contentsOf: flagsFilePath) else {
            assertionFailure("Flags file corrupted")
            return
        }
        guard let flags = try? JSONDecoder().decode([Flag].self, from: flagsData) else {
            assertionFailure("Flags file corrupted")
            return
        }
        self.flags = flags
    }
    
    func flag(for countryCode: String) -> String {
        let flags = self.flags.filter { return $0.code == countryCode }
        guard let flag = flags.first else {
            assertionFailure("No linked flag emoji, please add flag for: \(countryCode)")
            return Constants.notFoundFlag
        }
        return flag.emoji
    }
    
}
