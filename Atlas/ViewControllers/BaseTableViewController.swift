//
//  BaseTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/29/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    var dataSource: DataSource?
    
    func pushDetails(for country: Country) {
        guard let navigationController = navigationController else {
            assertionFailure("No navigation controller to push")
            return
        }
        let storyboard = UIStoryboard(name: SharedConstants.mainStoryboardName, bundle: nil)
        guard let detailsController = storyboard.instantiateViewController(withIdentifier: String(describing: CountryDetailsTableViewController.self)) as? CountryDetailsTableViewController else {
            assertionFailure("Check CountryDetailsTableViewController storyboard identifier")
            return
        }
        detailsController.dataSource = dataSource
        detailsController.country = country
        navigationController.pushViewController(detailsController, animated: true)
    }
}
