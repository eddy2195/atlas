//
//  FavoritesTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/27/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class FavoritesTableViewController: BaseCountriesListTableViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadFavoriteCountries()
    }
    
    private func reloadFavoriteCountries() {
        var favoriteCountries = [Country]()
        
        FavoritesService.shared.countriesAlphaCodes.forEach {
            guard let country = dataSource?.country(withCode: $0) else { return }
            favoriteCountries.append(country)
        }
        
        countries = favoriteCountries
        tableView.reloadData()
    }

}
