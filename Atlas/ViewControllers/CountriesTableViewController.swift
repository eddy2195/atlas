//
//  CountriesTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/27/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class CountriesTableViewController: BaseCountriesListTableViewController {

    var region: String? {
        didSet {
            guard let region = region, let countries = dataSource?.countries(in: region) else { return }
            self.countries = countries
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = region
    }
    
}
