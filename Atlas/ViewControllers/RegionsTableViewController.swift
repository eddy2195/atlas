//
//  RegionsTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/27/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class RegionsTableViewController: BaseTableViewController {
    
    private lazy var regions: NSOrderedSet? = {
        guard let dataSource = dataSource else {
            assertionFailure("No regions at point when the should be. Check Data provider.")
            return nil }
        return dataSource.regions
    }()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let regions = regions else {
            assertionFailure("No regions at point when the should be. Check Data provider.")
            return 0
        }
        return regions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.regionCellReuseIdentifier, for: indexPath)
        guard let regions = regions else {
            assertionFailure("No regions at point when the should be. Check Data provider.")
            return cell
        }
        guard let regionString = regions[indexPath.row] as? String else {
            assertionFailure("No regions at point when the should be. Check Data provider.")
            return cell
        }
        cell.textLabel?.text = regionString.isEmpty ? Constants.noNameRegion : regionString
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let regions = regions else {
            assertionFailure("No regions at the point of selecting one from list. Check data source.")
            return
        }
        guard let selectedRegion = regions[indexPath.row] as? String else {
            assertionFailure("Wrong objects type in regions collection")
            return
        }
        pushCountries(in: selectedRegion)
    }

    private func pushCountries(in region: String) {
        guard let navigationController = navigationController else {
            assertionFailure("No navigation controller to push")
            return
        }
        let storyboard = UIStoryboard(name: SharedConstants.mainStoryboardName, bundle: nil)
        guard let countriesViewController = storyboard.instantiateViewController(withIdentifier: String(describing: CountriesTableViewController.self)) as? CountriesTableViewController else {
            assertionFailure("Check CountriesTableViewController storyboard identifier")
            return
        }
        countriesViewController.dataSource = dataSource
        countriesViewController.region = region
        navigationController.pushViewController(countriesViewController, animated: true)
    }
}
