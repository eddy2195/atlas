//
//  SearchTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class SearchTableViewController: BaseCountriesListTableViewController {
    
    @IBOutlet private weak var searchBar: UISearchBar!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
}

extension SearchTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let dataSource = dataSource else { return }
        countries = dataSource.countries(withName: searchText)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
