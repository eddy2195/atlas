//
//  BaseCountriesListTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

class BaseCountriesListTableViewController: BaseTableViewController {
    
    var countries: [Country] = [Country]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: String(describing: CountryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CountryCell.self))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CountryCell.self), for: indexPath) as? CountryCell else {
            assertionFailure("No registered nib for CountryCell")
            return UITableViewCell()
        }
        cell.country = countries[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pushDetails(for: countries[indexPath.row])
    }
    
}
