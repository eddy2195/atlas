//
//  LoadingViewController.swift
//  Atlas
//
//  Created by Alex on 12/29/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class LoadingViewController: UIViewController {
    
    @IBOutlet private weak var activityIndicatiorView: UIActivityIndicatorView!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var tryAgainButton: UIButton!
    @IBOutlet private weak var proceedWithLocalDatabaseButton: UIButton!

    var dataSource: DataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dataSource = DataSource(with: self)
        dataSource.fetch()
        self.dataSource = dataSource
        
        proceedWithLocalDatabaseButton.isHidden = !dataSource.databaseExists
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let tabBarViewController = segue.destination as? UITabBarController,
            let controllers = tabBarViewController.viewControllers else {
                assertionFailure("Wrong application controllers hierarchy, check storyboard")
                return
        }
        
        controllers.forEach {
            guard let navigationController = $0 as? UINavigationController,
            let tableViewController = navigationController.viewControllers.first as? BaseTableViewController else {
                assertionFailure("Wrong application controllers hierarchy, check storyboard")
                return
            }
            tableViewController.dataSource = dataSource
        }
    }
    
    @IBAction func proccedWithLocalDatabase(_ sender: UIButton) {
        dataSource?.fetch(from: .filesystem)
    }

    @IBAction func tryAgain(_ sender: UIButton) {
        activityIndicatiorView.startAnimating()
        tryAgainButton.isHidden = true
        statusLabel.text = Constants.fetchingDatabaseString
        dataSource?.fetch(from: .server)
    }

}

extension LoadingViewController: FetchDelegate {
    func fetchingFinished(error: Error?) {
        activityIndicatiorView.stopAnimating()
        
        guard let error = error, let dataSource = dataSource else {
            performSegue(withIdentifier: Constants.regionsSegueIdentifier, sender: self)
            return
        }
        statusLabel.text = error.localizedDescription
        proceedWithLocalDatabaseButton.isHidden = !dataSource.databaseExists
        tryAgainButton.isHidden = false
    }
}
