//
//  CountryDetailsTableViewController.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit
import MapKit

final class CountryDetailsTableViewController: BaseTableViewController {
    
    @IBOutlet private weak var addToFavoritesButton: UIButton!
    
    var country: Country? {
        didSet {
            guard let country = country else { return }
            neighbours = dataSource?.neigbours(of: country)
        }
    }
    
    private var neighbours: [Country]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNibs()
        
        guard let country = country else { return }
        navigationItem.title = country.flag
        navigationItem.prompt = country.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let country = country else { return }
        addToFavoritesButton.isSelected = FavoritesService.shared.countriesAlphaCodes.contains(country.alpha3Code)
    }
    
    @IBAction private func favorite(_ sender: UIButton) {
        guard let country = country else { return }
        
        sender.isSelected = !sender.isSelected
        
        switch sender.isSelected {
            case true: FavoritesService.shared.add(country: country)
            case false: FavoritesService.shared.remove(country: country)
        }
    }
    
    private func registerNibs() {
        tableView.register(UINib(nibName: String(describing: CountryDetailsCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CountryDetailsCell.self))
        tableView.register(UINib(nibName: String(describing: CountryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CountryCell.self))
    }
    
}

extension CountryDetailsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let screenSection = Constants.Section(rawValue: section) else {
            assertionFailure("Section wasn't described in related Section enum")
            return 0
        }
        switch screenSection {
            case .details: return Constants.numberOfCellsInDetailsSection
            case .neighbours :
                guard let neighbours = neighbours else { return 0 }
                return neighbours.count
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let screenSection = Constants.Section(rawValue: section) else {
            assertionFailure("Section wasn't described in related Section enum")
            return nil
        }
        
        switch screenSection {
            case .details: return nil
            case .neighbours:
                guard let neighbours = neighbours, !neighbours.isEmpty else { return nil }
                return Constants.boardsWithText
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let screenSection = Constants.Section(rawValue: indexPath.section) else {
            assertionFailure("Section wasn't described in related Section enum")
            return UITableViewCell()
        }
        
        switch screenSection {
        case .details:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:CountryDetailsCell.self), for: indexPath) as? CountryDetailsCell else {
                assertionFailure("No registered nib for CountryDetailsCell")
                return UITableViewCell()
            }
            cell.country = country
            return cell
        case .neighbours:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CountryCell.self), for: indexPath) as? CountryCell else {
                assertionFailure("No registered nib for CountryCell")
                return UITableViewCell()
            }
            guard let neighbours = neighbours else {
                assertionFailure("No neighbour object for cell. Check data source")
                return cell
            }
            cell.country = neighbours[indexPath.row]
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let neighbours = neighbours else {
            assertionFailure("No neighbour object for selected cell. Check data source")
            return
        }
        pushDetails(for: neighbours[indexPath.row])
    }
    
}
