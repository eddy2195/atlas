//
//  CountryCell.swift
//  Atlas
//
//  Created by Alex on 12/29/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit

final class CountryCell: UITableViewCell {
    @IBOutlet private weak var flagLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var nativeNameLabel: UILabel!
    
    var country: Country? {
        didSet {
            guard let country = country else { return }
            flagLabel.text = country.flag
            nameLabel.text = country.name
            nativeNameLabel.text = country.nativeName
        }
    }
}
