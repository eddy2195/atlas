//
//  CountryDetailsCell.swift
//  Atlas
//
//  Created by Alex on 12/31/18.
//  Copyright © 2018 SH. All rights reserved.
//

import UIKit
import MapKit

final class CountryDetailsCell: UITableViewCell {
    
    @IBOutlet private weak var currencyLabel: UILabel!
    @IBOutlet private weak var languagesLabel: UILabel!
    @IBOutlet private weak var mapView: MKMapView!
    
    var country: Country? {
        didSet {
            guard let country = country else { return }
            setup(with: country)
        }
    }
    
    private func setup(with country: Country) {
        let currenciesNames = country.currencies.compactMap{$0.name}.joined(separator: "\n")
        currencyLabel.text = currenciesNames
        
        let languagesNames = country.languages.compactMap{$0.name}.joined(separator: "\n")
        languagesLabel.text = languagesNames
        
        if let latlng = country.latlng, let lat = latlng.first, let lon = latlng.last, mapView.annotations.isEmpty {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            mapView.addAnnotation(annotation)
            mapView.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)), animated: false)
        }
    }
}
